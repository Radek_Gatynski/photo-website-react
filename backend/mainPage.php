<?php 
function loadMainPageImages(){
	$path = './content/galleries/mainPage';
	$files = scandir($path);
	shuffle($files);
	$carusel = "<div id='carouselExampleSlidesOnly' class='carousel slide' data-ride='carousel'>
	<div class='carousel-inner' style='height:100vh'>";
	$i = 0;
	foreach($files as $file){
		if($file != "." && $file != ".." && $file != "thumbs"){
			if($i==0){
				$carusel = $carusel . "<div class='carousel-item active'><img class='d-block w-100' src='$path/$file' alt='$file'></div>";
			}else{
				$carusel = $carusel . "<div class='carousel-item'><img class='d-block w-100' src='$path/$file' alt='$file'></div>";
			}
			$i++;		//echo "<img src='$path/$file'  />";
		}
	}
	
	$carusel = $carusel . " 
	<a class='carousel-control-prev' href='#carouselExampleSlidesOnly' role='button' data-slide='prev'>
	<span class='carousel-control-prev-icon' aria-hidden='true'></span>
	<span class='sr-only'>Previous</span>
	</a>
	<a class='carousel-control-next' href='#carouselExampleSlidesOnly' role='button' data-slide='next'>
	<span class='carousel-control-next-icon' aria-hidden='true'></span>
	<span class='sr-only'>Next</span>
	</a>
	</div></div>";
	echo $carusel;
}
?>