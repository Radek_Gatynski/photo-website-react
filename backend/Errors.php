<?php 
    class Errors{
        public static $ConnectionError = "Nie można się połączyć z bazą danych";
        public static $loginFail = "Nie możesz się zalogować";
        public static $loginError = "Login jest niepoprawny";
        public static $nameEmpty = "Nazwa galerii jest pusta";
        public static $nameToShortOrTooLong = "Nazwa galerii jest za krótka albo za długa";
        public static $passwordIsEmpty = "Hasło jest puste";
        public static $passwordIsTooShortOrTooLong = "Długość hasła jest nieprawwidłowa";
        public static $passwordRegrex = "Hasło jest nieprawidłowe";
        public static $loginAdminError = "Nazwa użytkownika jest nieprawidłowa";
        public static $loginAdminIsEmpty = "Nazwa jest pusta";
        public static $NameIsAllreadyTaken = "Nazwa jest już zajęta spróbuj innej";
        public static $CanNotDeleteThisRecord = "Nie można skasować tego rekordu";
        public static $dodanoDoBazy = "Rekord dodany do bazy";
        public static $stworzonoFoldery = "Stworzono prawidłowo strukturę folderów";
        public static $bladPodczasTworzeniaFolderow = "Błąd podczas tworzenia folderów, wycofywanie zmian";
        public static $cantChangeImage = "Nie można zmienić zdjęcia";


        //panel admina
        public static $cantCreateNewGallery = "Nowa galeria nie została utworzona";
        public static $theStoryGallerieNameIsEmpty = "Nazwa galeri bądź histori jest pusta";
        public static $theStoryGallerieNameIsToLong = "Nazwa jest za długa";


        public static $shortNameIsEmpty = "Nazwa jest za krótka";
        public static $shortNameIsToLong = "Nazwa jest za długa";
        public static $shortNameWithoutSpaces = "Nazwa nie może zawierać spacji";


        public static $cantCreateNewStory = "Nowa historia nie została utworzona";
        public static $cantCreateThumb = "Nie można było utworzyć małego zdjęcia";


        //edycja 
        public static $cantEditStory = "Nie udało się edytować historii";


        public static $thumbWasChange = "Zdjęcie zostało zmienione";
        public static $thumbWasNotChange = "Zdjęcie nie zostało zmienione";
    }
?>