<?php 
class Account{
    private $pdo;
    private $errors;
    public function __construct($conn){
        $this->errors = array();
        $this->pdo = $conn->connect();
    }
   
    public function login($login, $password){
        $login = $this->validLogin($login);
        $password = $this->validPassword($password);
        if($login && $password){
            $sql = "Select * from galleries where shortName=?";
            if($stmt=$this->pdo->prepare($sql)){
                $stmt->bindParam(1,$login);
                if($stmt->execute()){
                    $galeria = $stmt->fetch();
                    if($galeria && password_verify($password, $galeria['security'])){
                        session_start();
                            $_SESSION['id'] = $galeria['id'];
                            $_SESSION['galleryName'] = $galeria['shortName'];
                            $_SESSION['longName'] = $galeria['description'];
                        return true;
                    }
                }
            }
        }
        array_push($this->errors, Errors::$loginFail);
        return false;
    }

    public function adminLogin($login, $password){
        $login = $this->validLogin($login);
        $password = $this->validPassword($password);
        $admin = 1;
        if($login && $password){
            $sql = "Select * from users where login=? and role=?";
            if($stmt=$this->pdo->prepare($sql)){
                $stmt->bindParam(1,$login);
                $stmt->bindParam(2,$admin);
                if($stmt->execute()){
                    $user = $stmt->fetch();
                    if($user && password_verify($password, $user['password'])){
                        session_start();
                            $_SESSION['id'] = $user['id'];
                            $_SESSION['name'] = $user['login'];
                            $_SESSION['admin'] = 'admin';
                        return true;
                    }
                }
            }
        }
        array_push($this->errors, Errors::$loginFail);
        return false;
    }


    private function validLogin($login){
        $login = trim($login);
        if(empty($login)){
            array_push($this->errors, Errors::$loginError);
            return false;
        }elseif(strlen($login)<5 || strlen($login>50)){
            array_push($this->errors, Errors::$nameToShortOrTooLong);
            return false;
        }else{
            return $login;
        }
    }
    private function validPassword($password){
        $password = trim($password);
        if(empty($password)){
           array_push($this->errors, Errors::$passwordIsEmpty);
            return false;
        }elseif(strlen($password)<8 || strlen($password)>50){
            array_push($this->errors, Errors::$passwordIsTooShortOrTooLong);
            return false;
        }elseif(!preg_match('/[^A-Za-z0-9]/', $password)){
            array_push($this->errors, Errors::$passwordRegrex);
            return false;
        }else{
            return $password;
        }
    }

    public function getError($msg_error){
        if(!in_array($msg_error, $this->errors)){
            $msg_error = "";
        }
        return $msg_error;
    }
}
?>