import React from 'react';
import './Loader.scss';
function Loader(){
    return(
        <div className="Loader">
            <header>
                <div className="loader">
                    <h2>Zglejszewski Wedding Photo</h2>
                </div>
            </header>
        </div>
    )
}
export default Loader;