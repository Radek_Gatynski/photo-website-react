import React from 'react';
import './NotFound.scss'
const NotFound = () => {
    return(
        <div className="NotFound">
            <header>Page not found
            <h1 className="center">404</h1>    
            </header>
            
        </div>
    )
}
export default NotFound;