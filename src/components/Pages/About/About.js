import React from 'react';
import oMnie from '../../../assets/images/o-mnie.jpg';
import "./About.scss";

const About = () => {
    return(
        <div className="container-grid" >
            <div className="Description">
                <h1>Cześć!</h1>
                <p>Skoro tu jesteście to znak, że potrzebujecie fotografa. Pozwólcie, że się przedstawię!
                    Nazywam się Przemek i od zawsze lubię robić zdjęcia. Pomysł na fotografię ślubną i rodzinną narodził się wraz z założeniem własnej rodziny. Od jakiegoś czasu sam jestem szczęśliwym tatą. Dzięki temu jeszcze bardziej zacząłem doceniać potęgę fotografii.
                    Zdjęcia na zawsze zatrzymują emocje i chwile, które często zdarzają się tylko raz w całym życiu. Zrobię wszystko co w mojej mocy, żeby Wasze fotografie były piękną pamiątką. Za kilkanaście lat nabiorą wyjątkowej mocy i będą budziły najlepsze wspomnienia.
                    Zaufajcie mi i upamiętnijcie ze mną najważniejsze chwile!
                </p>
                <i>Przemek Zglejszewski</i>
            </div>
        </div>
    )
}

export default About;