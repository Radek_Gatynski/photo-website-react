import React from 'react';
import './Welcome.scss';
import axios from 'axios';

class Welcome extends React.Component{
    render(){
        let ximage = {
            url:null
        };
        axios.get('https://miro.medium.com/max/1400/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg')
                .then((response)=>{
                    ximage.url =response.config.url;
                    debugger;
                })
        
        return(
            <div className="Welcome">
                <img src={ximage.url} width="400" height="400" />
            </div>
        )
    }
}
export default Welcome;