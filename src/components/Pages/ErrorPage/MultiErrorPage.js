import React from 'react';
import { render } from '@testing-library/react';

const MultiErrorPage = (props) =>{
        return(
            <div>
                <header>
                    <h3>{props.error.header}</h3>
                </header>
                <div>
                    <p>Kod błędu: {props.error.code}</p>
                    <p>Treść: {props.error.content}</p>
                </div>
            </div>
        )
}

export default MultiErrorPage;
