import React from 'react';
import './Login.scss';
class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            password: ''
        }
        this.handeChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event){
      let  name = event.target.name;
      this.setState({
          [name]: event.target.value
      })
    } 
    handleSubmit(event) {
        event.preventDefault();
        
      }
    render(){ 
        return(
            <div className="Login">
                <h1>Login</h1>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="name">
                        Name:
                    </label>
                    <input id="name" name="name" type="text" value={this.state.name} onChange={this.handleChange} required/>
                    <label htmlFor="password">
                        Password
                    </label>
                    <input id="password" name="password" type="password" value={this.state.password} onChange={this.handleChange} required/>
                    <input type="submit" value="Wyślij" />
                </form>
            </div>
        )
    }
}
export default Login;