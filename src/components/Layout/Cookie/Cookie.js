import React from 'react';
import './Cookie.css';


export default class Cookie extends React.Component {
    constructor(props){
        super(props);
        this.setCookies = this.setCookies.bind(this);
    }
    setCookies(){
      let dataTimeNow = new Date();
      dataTimeNow.setDate(dataTimeNow.getDate()+90)
      dataTimeNow = String(dataTimeNow).split(" ");
      let timeRegion = dataTimeNow[5].split('+')[0];
      document.cookie = `expires=${dataTimeNow[0]}, ${dataTimeNow[2]} ${dataTimeNow[1]} ${dataTimeNow[3]} ${dataTimeNow[4]} ${timeRegion}`
    }
    render(){
        let isCookieSet = decodeURIComponent(document.cookie).split(";");
        if(!isCookieSet){
            debugger;
            return(
                <div className="Cookie">
                    <p>This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you.</p>
                        {/*<Button typeClass="primary" description="Accept" onClick={this.setCookies} /> */}
                        <button onClick={this.setCookies}>Accept</button>
                </div>
            )
        }else{
            return null;
        }
    }
}