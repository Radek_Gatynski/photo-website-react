import React from 'react';
import Navitem from './Navitem';
import './Navbar.scss';
import {Link} from 'react-router-dom'
import logo from './../../../assets/images/logo-white.svg'
class Navbar extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            openNav: false
        }
    }
    render(){
        return(
            <div className="navbar">
                <div>
                    <img src={logo} height="60" width="224" alt="logo" />
                </div>
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/about">O mnie</Link></li>
                        <li><Link to="/historie">Historie</Link></li>
                        <li><Link to="/kontakt">Kontakt</Link></li>
                        <li><Link to="/login">Logowanie</Link></li>
                    </ul>
            </div>
        )
    }
}
export default Navbar;