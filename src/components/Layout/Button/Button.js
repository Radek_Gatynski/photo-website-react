import React from 'react';
import './Buttons.scss';

function Button(props){
    let styles = "button " + props.typeClass;
    return(
        <React.Fragment>
            <button className={styles}>{props.description}</button>
        </React.Fragment>
    )
}
export default Button;