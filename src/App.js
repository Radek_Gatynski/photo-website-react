import React, { Suspense, lazy } from 'react';
import {Route,Switch, Redirect} from 'react-router-dom';
import Cookie from './components/Layout/Cookie/Cookie';
import Loader from './components/Pages/Loader/Loader';
import Navbar from './components/Layout/Navbar/Navbar';
const Welcome = lazy(() => import('./components/Pages/Welcome/Welcome')); 
const Login = lazy(() => import('./components/Pages/Login/Login'));
const SigneiIn = lazy(() => import('./components/Pages/SignIn/SignIn'));
const NotFound = lazy(() => import("./components/Pages/NotFound/NotFound"));
const About = lazy(() => import("./components/Pages/About/About"));

class App extends React.Component{
  render(){
    return (
      <div className="App">
        <Suspense fallback={Loader}>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Welcome} />
            <Route exact path="/signein" component={SigneiIn} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/about" component={About} />
            <Route exact path="/404" component={NotFound} />
            <Redirect from="*" to="/404" />
          </Switch>
        </Suspense>
        <Cookie />
      </div>
    );
  }
}

export default App;
